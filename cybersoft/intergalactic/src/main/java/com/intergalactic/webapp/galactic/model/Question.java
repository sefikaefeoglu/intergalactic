package com.intergalactic.webapp.galactic.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Question")
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false)
	private long id;
	
	@Column(name = "questionString")
    private String questionString;
	
	@Column(name = "symbollList", nullable = false)
    private String symbollList;
	
	@Column(name = "amount")
    private float amount;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestionString() {
		return questionString;
	}

	public void setQuestionString(String questionString) {
		this.questionString = questionString;
	}

	public String getsymbollList() {
		return symbollList;
	}

	public void setsymbollList(String symbollList) {
		this.symbollList = symbollList;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Question(String symbollList) {
		super();
		this.symbollList = symbollList;
	}

	public Question(String questionString, String symbollList, float amount) {
		super();
		this.questionString = questionString;
		this.symbollList = symbollList;
		this.amount = amount;
	}

	public Question(String symbollList, float amount) {
		super();
		this.symbollList = symbollList;
		this.amount = amount;
	}

	public Question(String questionString, String symbollList) {
		super();
		this.questionString = questionString;
		this.symbollList = symbollList;
	}
	

}
