package com.intergalactic.webapp.galactic.repository;



import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.intergalactic.webapp.galactic.model.Question;

@Transactional
public interface QuestionDAO extends CrudRepository<Question, Long> {
	@Modifying(clearAutomatically = true)
	@Query("UPDATE Question q SET q.amount=:amount WHERE q.id =:id")
    void setAmountForId(@Param("amount")float amount,@Param("id")Long id );
	
}
