package com.intergalactic.webapp.galactic.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.intergalactic.webapp.galactic.model.Symboll;


@Transactional
public interface SymbollDAO extends CrudRepository<Symboll, Long> {
	
	
}
