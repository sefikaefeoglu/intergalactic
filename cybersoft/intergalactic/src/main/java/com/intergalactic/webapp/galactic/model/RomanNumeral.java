package com.intergalactic.webapp.galactic.model;

import javax.persistence.*;



@Entity
@Table(name = "RomanNumeral")
public class RomanNumeral {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false)
	private long id;
	
	@Column(name = "symboll", nullable = false)
    private String symboll;
	
	@Column(name = "value", nullable = false)
    private int value;
	
	public String getSymboll() {
		return symboll;
	}
	public void setSymboll(String symboll) {
		this.symboll = symboll;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public RomanNumeral() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RomanNumeral(String symboll, int value) {
		super();
		this.symboll = symboll;
		this.value = value;
	}
	
	
	

}
