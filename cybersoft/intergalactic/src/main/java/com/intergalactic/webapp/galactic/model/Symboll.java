package com.intergalactic.webapp.galactic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Symboll")
public class Symboll {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false)
	private long id;
	
	@Column(name = "name", nullable = false)
    private String name;
	
	
	@Column(name = "value", nullable = false)
    private String symboll;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSymboll() {
		return symboll;
	}

	public void setSymboll(String symboll) {
		this.symboll = symboll;
	}

	public Symboll(String name, String symboll) {
		super();
		this.name = name;
		this.symboll = symboll;
	}

	public Symboll() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

}
